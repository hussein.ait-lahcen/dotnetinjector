// dllmain.cpp : D�finit le point d'entr�e pour l'application DLL.
#include "stdafx.h"

using namespace std;

extern "C" __declspec(dllexport) void __cdecl Load();
void Load()
{
	HRESULT hr;
	ICLRMetaHost *pMetaHost = NULL;
	ICLRRuntimeInfo *pRuntimeInfo = NULL;
	ICLRRuntimeHost *pClrRuntimeHost = NULL;

	hr = CLRCreateInstance(CLSID_CLRMetaHost, IID_ICLRMetaHost, (LPVOID*)&pMetaHost); 
	hr = pMetaHost->GetRuntime(L"v4.0.30319", IID_ICLRRuntimeInfo, (LPVOID*)&pRuntimeInfo);
	hr = pRuntimeInfo->GetInterface(CLSID_CLRRuntimeHost,
		IID_ICLRRuntimeHost, (LPVOID*)&pClrRuntimeHost); 
	hr = pClrRuntimeHost->Start();

	DWORD pReturnValue;
	hr = pClrRuntimeHost->ExecuteInDefaultAppDomain(
		L"C:/Users/haitlahcen/Documents/Visual Studio 2013/Projects/Test.Injector/Test.Managed.Processor/bin/Debug/Test.Managed.dll",
		L"Test.Managed.Processor",
		L"EntryPoint",
		L"Inside",
		&pReturnValue);

	pMetaHost->Release();
	pRuntimeInfo->Release();
	pClrRuntimeHost->Release();
}