﻿using ManagedWinapi.Windows;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Test.Managed
{
    public partial class MainForm : Form
    {      
        [DllImport("kernel32.dll")]
        static extern uint GetLastError();

        public MainForm()
        {
            InitializeComponent();            
        }

        private unsafe void MainForm_Load(object sender, EventArgs e)
        {
            var thisWindow = new SystemWindow(Handle);
            var subWindows = SystemWindow.AllCurrentProcessWindows.Where(window => window.ClassName != "WinDevObject" || window.Title != "");
            
            MouseInterceptor.OnMouseClick += (data) =>
            {
                var window = SystemWindow.FromPointCurrentProcess(data.pt.x, data.pt.y);
                if (window == null)
                    return;

                if (window.HWnd == Handle || window.IsDescendantOf(thisWindow))
                    return;

                foreach(var subWindow in subWindows)
                    if (window.IsDescendantOf(subWindow))
                        return;

                richTxtConsole.AppendText("==========================\n");
                richTxtConsole.AppendText(window.ClassName + "\n");
                richTxtConsole.AppendText(window.Title + "\n");
                richTxtConsole.AppendText(window.DialogID + "\n");
            };
        }
    }
}
