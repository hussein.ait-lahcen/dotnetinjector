﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Test.Managed
{
    public static class Processor
    {
        public const int EXIT_SUCCESS = 0;

        public static int EntryPoint(string args)
        {
            MouseInterceptor.Start();
            using (var form = new MainForm())
            {
                Application.EnableVisualStyles();
                Application.Run(form);
            }
            MouseInterceptor.Stop();
         
            return EXIT_SUCCESS;
        }
    }
}
