// Test.Injector.cpp�: d�finit le point d'entr�e pour l'application console.
//

#include "stdafx.h"

using namespace std;

DWORD_PTR InjectThread(const HANDLE hProcess, const LPVOID function, const wstring& argument)
{
	LPVOID baseAddress = VirtualAllocEx(hProcess, NULL, sizeof(TCHAR)*argument.length(),
		MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);

	BOOL isSucceeded = WriteProcessMemory(hProcess, baseAddress, argument.c_str(),
		sizeof(TCHAR)*argument.length(), NULL);

	HANDLE hThread = CreateRemoteThread(hProcess, NULL, 0,
		(LPTHREAD_START_ROUTINE)function, baseAddress, NULL, 0);

	WaitForSingleObject(hThread, INFINITE);

	VirtualFreeEx(hProcess, baseAddress, 0, MEM_RELEASE);

	DWORD exitCode = 0;
	GetExitCodeThread(hThread, &exitCode);

	CloseHandle(hThread);

	return exitCode;
}

DWORD_PTR GetRemoteModuleHandle(const int processId, const wchar_t* moduleName)
{
	MODULEENTRY32 me32;
	HANDLE hSnapshot = INVALID_HANDLE_VALUE;

	me32.dwSize = sizeof(MODULEENTRY32);
	hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, processId);

	if (!Module32First(hSnapshot, &me32))
	{
		CloseHandle(hSnapshot);
		return 0;
	}

	while (wcscmp(me32.szModule, moduleName) != 0 && Module32Next(hSnapshot, &me32));

	CloseHandle(hSnapshot);

	if (wcscmp(me32.szModule, moduleName) == 0)
		return (DWORD_PTR)me32.modBaseAddr;

	return 0;
}

DWORD_PTR GetFunctionOffset(const wstring& library, const char* functionName)
{
	HMODULE hLoaded = LoadLibraryW(library.c_str());

	void* lpInject = GetProcAddress(hLoaded, functionName);
	if (lpInject == NULL)
	{
		auto error = GetLastError();
		FreeLibrary(hLoaded);
		return 0;
	}

	DWORD_PTR offset = (DWORD_PTR)lpInject - (DWORD_PTR)hLoaded;

	FreeLibrary(hLoaded);
	return offset;
}

DWORD GetProcessByName(LPWSTR name)
{
	DWORD pid = -1;

	HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	PROCESSENTRY32 process;
	ZeroMemory(&process, sizeof(process));
	process.dwSize = sizeof(process);

	if (Process32First(snapshot, &process))
	{
		do
		{
			if (wcsstr(process.szExeFile, name))
			{
				pid = process.th32ProcessID;
				break;
			}
		} 
		while(Process32Next(snapshot, &process));
	}

	CloseHandle(snapshot);

	return pid;
}

BOOL Inject(LPWSTR procName, wstring dllPath)
{

	DWORD procID = GetProcessByName(procName);
	HANDLE hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, procID);
	if (hProcess == NULL)
	{
		printf("Specified process couldn't be found.\n");
		return FALSE;
	}

	auto loadLibAddr = GetProcAddress(GetModuleHandleW(TEXT("Kernel32")), "LoadLibraryW");
	if (loadLibAddr == NULL)
	{
		printf("LoadLibraryW not found in kernel32.dll.\n");
		return FALSE;
	}

	InjectThread(hProcess, loadLibAddr, dllPath);

	DWORD_PTR hBootstrap = GetRemoteModuleHandle(procID, L"Test.Bootstrap.dll");
	DWORD offset = GetFunctionOffset(dllPath, "Load");
	DWORD_PTR fnImplant = hBootstrap + offset;

	InjectThread(hProcess, (LPVOID)fnImplant, L"LOL");

	FARPROC fnFreeLibrary = GetProcAddress(GetModuleHandle(L"Kernel32"), "FreeLibrary");
	CreateRemoteThread(hProcess, NULL, 0, (LPTHREAD_START_ROUTINE)fnFreeLibrary, (LPVOID)hBootstrap, NULL, 0);

	CloseHandle(hProcess);

	return TRUE;
}

int _tmain(int argc, char** argv[])
{
	if(!Inject(L"ColorWinV2.exe", L"C:/Users/haitlahcen/Documents/Visual Studio 2013/Projects/Test.Injector/Debug/Test.Bootstrap.dll"))
		getchar();
	return EXIT_SUCCESS;
}

